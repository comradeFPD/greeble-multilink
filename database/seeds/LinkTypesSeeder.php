<?php

use Illuminate\Database\Seeder;

class LinkTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'vk',
                'pattern' => 'https://vk.com/',
                'icon' => 'http://e-class.pskova.ru/wp-content/uploads/2020/03/vkontakte_logo.png',
            ],
            [
                'title' => 'facebook',
                'pattern' => 'https://facebook.com/'
            ],
            [
                'title' => 'instagram',
                'pattern' => 'https://instagram.com/',
                'icon' => 'https://upload.wikimedia.org/wikipedia/commons/a/a5/Instagram_icon.png'
            ],
            [
                'title' => 'whatsapp',
                'pattern' => 'https://wa.me/',
                'icon' => 'https://vikats.com/ropa-femenina/iconos-de-redes-sociales-03.png'
            ],
            [
                'title' => 'site',
                'pattern' => '',
                'icon' => 'https://www.ketenstandaard.nl/cms_media/leden/195/logo_icon_325x325.png'
            ]
        ];
        foreach ($data as $item){
            \App\LinkType::create($item);
        }
    }
}
