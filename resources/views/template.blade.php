@extends('layouts.app')
@section('title', 'Шаблон')
<link rel="stylesheet" href="css/profile.css">
@if($user && $user->userTemplate)
    <style>
        @if($user->userTemplate->theme == 'dark')
            .template{
            background-color: #222222 ;
        }
        .text_bykh{
            color: #fff;
            background-color: #222222 ;
        }
            @endif
    </style>
    @endif
@section('content')
    <div class="container-fluid">
        <header>
            <div class="template">
                <div class="temp_block">

                    <div class="header__bg">
                        <img src="https://avatars.mds.yandex.net/get-pdb/2748472/e542422e-16ed-4439-8375-485e33a17e2d/s1200" alt="">
                    </div>

                    <div class="text_bykh">
                        <p class='text-center'>{{ $user->userTemplate ? $user->userTemplate->description : 'Здесь будет текст который вы разместили в описании' }}</p>
                    </div>

                    <div class="links_socials">
                        @if($user)
                            @foreach($user->links as $link)
                                <p><a href="{{ $link->link }}" target='_blank' class='btn btn-block btn-md btn-warning'>{{ $link->type->title }}</a></p>
                                @endforeach
                            @endif
                    </div>

                </div>
            </div>
            <p class='text-center'><a href="/" style='color:#222;font-size:14px;'>сделано с помощью <b>BYKH.ru</b></a></p>
        </header>
    </div>

    <div class="product__main">
        <div class="container">
            <div class="row">



            </div>
        </div>
    </div>
    @endsection
