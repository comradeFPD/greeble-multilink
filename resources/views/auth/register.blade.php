@extends('layouts.app')
@section('title', 'Регистрация')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <h3 class='text-center'><a href="/" style='padding:20px;color:#fff;display: block;'>«Бухта»</a></h3>
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">Напишите свою почту</h5>
                        <form class="form-signin" action="{{ route('register') }}" method="POST">
                            @csrf
                            <div class="form-label-group">
                                <input type="email" id="inputEmail" name="email" class="form-control" required autofocus>
                                <label for="inputEmail">Ваша почта</label>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Зарегистрироваться <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
