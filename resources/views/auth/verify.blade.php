@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <h3 class='text-center'><a href="/" style='padding:20px;color:#fff;display: block;'>«Бухта»</a></h3>
                <div class="card card-signin my-5">
                    <div class="card-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                {{ __('A fresh verification link has been sent to your email address.') }}
                            </div>
                        @endif
                        <p class='text-center' style='padding:20px 0;'><img src="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/72/apple/237/fire_1f525.png" alt=""></p>
                        <h5 class="card-title text-center">Проверьте пожалуйста свою почту <br/><b>{{ \Illuminate\Support\Facades\Auth::user()->email }}</b> и подтвердите ее</h5>
                            <p>Если вы не получили письмо</p>
                            <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                                @csrf
                                <button type="submit" class="btn btn-link p-0 m-0 align-baseline">Нажмите здесь чтобы запросить новое.</button>.
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
