@extends('layouts.app')
@section('content')
    @if($errors->any())
        <div role="alert" class="alert alert-danger alert-dismissible fade show">{{ $errors->first('password') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button></div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <h3 class='text-center'><a href="/" style='padding:20px;color:#fff;display: block;'>«Бухта»</a></h3>
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">Придумайте пароль и сохраните его</h5>
                        <form class="form-signin" action="{{ route('password.new') }}" method="post">
                            @csrf
                            <div class="form-label-group">
                                <input type="password" id="inputPassword" name="password" class="form-control" required>
                                <label for="inputPassword">Ваш пароль</label>
                            </div>
                            <div class="form-label-group">
                                <input type="password" id="inputPassword2" name="password_confirmation" class="form-control" required>
                                <label for="inputPassword2">Еще раз</label>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Сохранить <i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
