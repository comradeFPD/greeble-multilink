@extends('layouts.app')
@section('title', 'Авторизация')
@section('content')
    @if($errors->any())
        <div role="alert" class="alert alert-danger alert-dismissible fade show">{{ $errors->first('email') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button></div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <h3 class='text-center'><a href="/" style='padding:20px;color:#fff;display: block;'>«Бухта»</a></h3>
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">Войдите в аккаунт</h5>
                        <form class="form-signin" action="{{ route('login') }}" method="post">
                            @csrf
                            <div class="form-label-group">
                                <input type="email" value="{{ old('email') }}" id="inputEmail" class="form-control" name="email" required autofocus>
                                <label for="inputEmail">Ваша почта</label>
                            </div>
                            <div class="form-label-group">
                                <input type="password" id="inputPassword" class="form-control" name="password" required>
                                <label for="inputPassword">Ваш пароль</label>
                            </div>

                            <div class="custom-control custom-checkbox mb-3">
                                <a href="{{ route('password.request') }}">Забыли пароль?</a>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Войти <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
