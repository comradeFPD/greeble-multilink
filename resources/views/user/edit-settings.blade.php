@extends('layouts.app')
@include('layouts._nav')
@section('title', 'Создание ссылок')
<link rel="stylesheet" href="/css/first-setting.css">
<link rel="stylesheet" href="/css/register.css">
<link rel="stylesheet" href="/css/profile.css">
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <h3 class='text-center'><a href="/" style='padding:20px;color:#fff;display: block;'>«Бухта»</a></h3>
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <form class="form-signin" action="{{ route('settings.save') }}" method="post">
                            @csrf
                            <h5 class="card-title text-center">Придумайте логин для нашего сервиса</h5>
                            <div class="first__link_left col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="login" value="{{ $user->login ?? '' }}" placeholder="Например: batraiofficial">
                                </div>
                            </div>
                            <h5 class="card-title text-center">Введите логины своих соц.сетей</h5>
                            <div class="first__links">
                                <div class="row">
                                    <div class="first__link_left col-md-2">
                                        <img src="http://e-class.pskova.ru/wp-content/uploads/2020/03/vkontakte_logo.png" alt="">
                                    </div>
                                    <div class="first__link_left col-md-10">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="vk" value="{{ $links != null ? $links->where('type', 'vk')->first()['link'] : ''}}" placeholder="Введите id или логин">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="first__link_left col-md-2">
                                        <img src="https://upload.wikimedia.org/wikipedia/commons/a/a5/Instagram_icon.png" alt="">
                                    </div>
                                    <div class="first__link_left col-md-10">
                                        <div class="form-group">
                                            <input type="text" class="form-control" value="{{ $links != null ? $links->where('type', 'instagram')->first()['link'] : ''}}" name="instagram" placeholder="Введите id или логин">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="first__link_left col-md-2">
                                        <img src="https://i.pinimg.com/originals/1a/72/d4/1a72d48d6090f689bd3bc8d59e088516.png" alt="">
                                    </div>
                                    <div class="first__link_left col-md-10">
                                        <div class="form-group">
                                            <input type="text" class="form-control" value="{{ $links != null ? $links->where('type', 'facebook')->first()['link'] : ''}}" name="facebook" placeholder="Введите id или логин">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p style='margin:20px 0 0 0;'>
                                <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit"> Сохранить </button>
                            </p>
                        </form>

                    </div>
                </div>
            </div>
        </div>
@endsection
