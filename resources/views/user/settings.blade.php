@extends('layouts.app')
<link rel="stylesheet" href="css/first-setting.css">
<link rel="stylesheet" href="css/register.css">
@section('title', 'Создание ссылок')
@section('content')
    @if($errors->any())
        <div role="alert" class="alert alert-danger alert-dismissible fade show">{{ $errors->first() }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button></div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <h3 class='text-center'><a href="/" style='padding:20px;color:#fff;display: block;'>«Бухта»</a></h3>
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <form class="form-signin" action="{{ route('settings.save') }}" method="post">
                            @csrf
                            <h5 class="card-title text-center">Придумайте логин для нашего сервиса</h5>
                            <div class="first__link_left col-md-12">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="login__setting"><i class="fa fa-lock" aria-hidden="true" style='padding-right: 3px;color: #626365;'></i> bykh.ru/</span>
                                    </div>
                                    <input type="text" value="{{ old('login') }}" class="form-control" aria-describedby="login__setting" name="login" placeholder="Например: kuhni_maykop">
                                </div>
                            </div>



                            <h5 class="card-title text-center">Напишите номер, который прикреплен к WhatsApp</h5>
                            <div class="first__links">
                                <div class="row">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" id="whatsapp__setting"><img src="https://vikats.com/ropa-femenina/iconos-de-redes-sociales-03.png" alt="" style='height: 30px;'></span>
                                        </div>
                                        <input value="{{ old('whatsapp') }}" type="text" class="form-control" aria-describedby="whatsapp__setting" name="whatsapp" placeholder="Введите номер с 7" style='height: 45px;' maxlength="30">
                                    </div>
                                    <small>Формат: 79004445566</small>
                                </div>
                            </div>
                            <p style='margin:20px 0 0 0;'>
                                <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit"> Далее <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                            </p>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('input[name="whatsapp"]').inputmask('+###########');
            })
        </script>
    @endsection
