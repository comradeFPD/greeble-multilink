@section('nav')
    <nav class="megamenu">
        <ul class="megamenu-nav d-flex justify-content-center" role="menu">
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/home') }}">
                    <i class="fa fa-user-circle" aria-hidden="true"></i> Профиль
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('user.edit-settings') }}">
                    <i class="fa fa-cog" aria-hidden="true"></i> Настройки
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"  onclick="event.preventDefault();
                                                                       document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out" aria-hidden="true"></i> Выход
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </nav>
    @endsection
