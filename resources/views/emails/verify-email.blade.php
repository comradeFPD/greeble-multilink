@component('mail::message')
Спасибо за регистрацию в Бухте, осталось подтвердить свою почту


@component('mail::button', ['url' => $verifyUrl])
Подтвердить почту
@endcomponent


@endcomponent
