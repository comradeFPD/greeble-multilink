<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewPasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthNewPasswordController extends Controller
{

    public function getCreateNewPassword()
    {
        return response()->view('auth.passwords.new');
    }

    public function postCreateNewPassword(NewPasswordRequest $request)
    {
        $user = Auth::user();
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect(route('settings'));
    }
}
