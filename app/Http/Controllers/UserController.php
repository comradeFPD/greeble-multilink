<?php

namespace App\Http\Controllers;

use App\LinkService;
use App\LinkType;
use App\User;
use App\UserLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function getSettingsForm()
    {
        return response()->view('user.settings');
    }

    public function postSaveLinks(Request $request)
    {
        $request->validate([
            'login' => 'required|unique:users',
            'whatsapp' => ['required', function($attribute, $value, $fail){
                if(!preg_match('/[+]\d{11}/m', $value)){
                    $fail($attribute. ' имеет не верный формат');
                }
            }]
        ]);
        $user = Auth::user();
        $data = $request->except(['_token', 'login']);
        $service = new LinkService($user);
        $user->login = $request->login;
        $user->save();
        if($user->links->isNotEmpty()){
            $service->updateLinks($data);
        } else {
            $service->createLink($data);
        }
        return redirect('/home');
    }

    public function getShowTemplate()
    {
        return response()->view('template');
    }

    public function getShowUserTemplate($login)
    {
        $user = User::whereLogin($login)->firstOrFail();
        return response()->view('template', [
            'user' => $user
        ]);
    }

    public function postSaveTemplate(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'profile_name' => 'required|unique:user_templates,profile_name',
            'login' => "unique:users,login,{$user->id}"
        ]);
        if ($user->userTemplate) {
            $template = $user->userTemplate;
        } else {
            $template = $user->userTemplate()->create([
                'profile_name' => $request->profile_name
            ]);
        }

        $template->description = $request->description;
        $template->profile_name = $request->profile_name;
        $user->login = $request->login ?? $user->login;
        $user->save();
        $template->save();
        return redirect()->back();
    }

    public function getUserSettings()
    {
        $user = Auth::user();
        $links = $user->links->map(function (UserLink $link){
            $resp = [
                'link' => collect(explode('/', $link->link))->last(),
                'type' => $link->type->title
            ];
            return $resp;
        });
        return response()->view('user.edit-settings', [
            'user' => $user,
            'links' => $links
        ]);
    }

    public function postChangeColor(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'avatar' => 'image'
        ]);
        if(!$user->userTemplate){
           $template =  $user->userTemplate()->create([
                'profile_name' => $user->login
            ]);
        } else {
            $template = $user->userTemplate;
        }
        $template->theme = $request->car;
        $template->save();
        if($request->avatar){
            $user->avatar = Storage::disk('public')->put('/users/avatars', $request->avatar);
            $user->save();
        }
        return redirect()->back();
    }

    public function postUpdateLinks(Request $request)
    {
        $user = Auth::user();
        $service = new LinkService($user);
        $request->validate([
            'whatsapp' => ['required', function($attribute, $value, $fail){
                if(!preg_match('/[+]\d{11}/m', $value)){
                    $fail($attribute. ' имеет не верный формат');
                }
            }],
        ]);
        $data = $request->except('_token');
        foreach ($data as $key => $value) {
            if($user->links->pluck('type_id')->contains(LinkType::whereTitle([$key])->first()->id)){
                $service->updateLinks([$key => $value]);
            } else {
                $service->createLink([$key => $value]);
            }
        }
        return redirect()->back();
    }
}
