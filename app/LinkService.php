<?php


namespace App;


class LinkService
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function createLink($data)
    {
        $types = LinkType::all();
        foreach ($data as $key => $value){
            if($types->contains('title', '=', $key)){
                $type = $types->where('title', '=', $key)->first();
                $this->user->links()->create([
                    'type_id' => $type->id,
                    'link' => $type->pattern.$value
                ]);
            }
        }
    }

    public function updateLinks($data)
    {
        $links = $this->user->links;
        foreach ($data as $key => $item){
            $links->each(function ($link) use ($key, $item){
                if($link->type->title == $key)
                    $link->link = $link->type->pattern.$item;
                $link->save();
            });
        }
    }
}
