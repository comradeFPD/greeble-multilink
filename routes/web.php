<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);


Route::middleware('verified')->group(function(){
    Route::get('/password', 'AuthNewPasswordController@getCreateNewPassword')->name('password.form');
    Route::post('/new-password', 'AuthNewPasswordController@postCreateNewPassword')->name('password.new');
    Route::get('/settings', 'UserController@getSettingsForm')->name('settings');
    Route::post('/settings', 'UserController@postSaveLinks')->name('settings.save');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/show-template', 'UserController@getShowTemplate')->name('template.show');
    Route::post('/save-template', 'UserController@postSaveTemplate')->name('template.save');
    Route::get('/user/settings', 'UserController@getUserSettings')->name('user.edit-settings');
    Route::post('/user/change-color', 'UserController@postChangeColor')->name('user.change-color');
    Route::post('/user/update-links', 'UserController@postUpdateLinks')->name('user.update-links');
});
Route::get('/{login}', 'UserController@getShowUserTemplate')->name('template.user');
